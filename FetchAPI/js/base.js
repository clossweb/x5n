var main = {
    class: {
        switchPage: function () {
            this.base_class = 'content-block';
            this.active_class = 'active';

            this.go = function (page) {
                main.class.remove(document.getElementsByClassName(this.base_class), this.active_class);
                main.class.add(document.getElementsByClassName(this.base_class + ' ' + page), this.active_class);
            };
        },
        replace: function (elements, oldClass, newClass) {
            main.class.remove(elements, oldClass);
            main.class.add(elements, newClass);
        },
        get: function (elements) {
            var classAll = [];
            for (var i = 0; i < elements.length; i++) {
                try {
                    classAll = classAll.concat(elements[i].getAttribute('class').split(/[\s]+/));
                } catch (error) {

                }
            }
            return classAll;
        },
        has: function (elements, className) {
            for (var i = 0; i < elements.length; i++) {
                if (this.get([elements[i]]).indexOf(className) >= 0) {
                    return true;
                }
            }
            return false;
        },
        add: function (elements, className) {
            var getAllClass = [];

            for (var i = 0; i < elements.length; i++) {
                getAllClass = this.get([elements[i]]);
                if (getAllClass.indexOf(className) < 0) {
                    getAllClass.push(className);
                    elements[i].setAttribute('class', getAllClass.join(' '));
                }
            }
        },
        remove: function (elements, className) {
            var getAllClass = [];

            for (var i = 0; i < elements.length; i++) {
                getAllClass = this.get([elements[i]]);
                if (getAllClass.indexOf(className) >= 0) {
                    getAllClass.splice(getAllClass.indexOf(className), 1);
                    elements[i].setAttribute('class', getAllClass.join(' '));
                }
            }
        }
    },
    add: function (op, add) {
        var create = {
            tagOBJ: (op || {}).element || null,
            tagChildOBJ: {},
            textNODE: ''
        };

        if (Array.isArray(add)) {

            for (var index in add) {
                create.tagChildOBJ = document.createElement(add[index].tag);

                for (var attr_key in add[index].attr) {
                    create.tagChildOBJ.setAttribute(attr_key, add[index].attr[attr_key]);
                }

                if (typeof add[index].content == 'string') {
                    create.textNODE = document.createTextNode(add[index].content);
                    create.tagChildOBJ.appendChild(create.textNODE);
                } else if (Array.isArray(add[index].content)) {
                    create.tagChildOBJ = this.add({element: create.tagChildOBJ}, add[index].content);
                }

                if (create.tagOBJ) {
                    if (op.prepend) {
                        create.tagOBJ.insertBefore(create.tagChildOBJ, create.tagOBJ.children[0]);
                        continue;
                    }
                    create.tagOBJ.appendChild(create.tagChildOBJ);
                    continue;
                }
                create.tagOBJ = create.tagChildOBJ;
            }

            return create.tagOBJ;
        }
        return this.add(op, [add]);
    },
    css: {
        sheets: document.styleSheets,
        list: {},

        add: function (style, id) {
            var lastSheet = this.sheets[document.styleSheets.length - 1];

            if (lastSheet.cssRules) {
                lastSheet.insertRule(style, lastSheet.cssRules.length);

                if (id) {
                    if (this.list[id]) {
                        this.list[id].push(lastSheet.cssRules.length - 1);
                        return;
                    }
                    this.list[id] = [lastSheet.cssRules.length - 1];
                }
            }
        },
        remove: function (id) {
            var lastSheet = this.sheets[document.styleSheets.length - 1];

            if (lastSheet.cssRules && id) {
                for (var index = (this.list[id] || {length: 0}).length - 1; index >= 0; index--) {
                    lastSheet.removeRule(this.list[id][index]);
                }
                delete this.list[id];
            }
        }
    },
    typeTo: {
        OBJ_ARR: function (object, index_depth) {
            var arr = [];
            for (var index in object) {
                arr.push(function () {
                    var _this = object[index];
                    for (var index_inner in index_depth) {
                        _this = _this[index_depth[index_inner]];
                    }
                    return _this;
                }());
            }
            return arr;
        }
    },
    number: {
        random: function (min, max) {
            return (Math.random() * (max - min) + min);
        }
    },
    rule: function () {
        this.form_fail = [];
        this.form_rules = {};

        this.getClassRule = function (element) {
            for (var key in this.form_rules) {
                if (main.class.has([element], key)) {
                    return {_class: key, _value: this.form_rules[key]};
                }
            }

            return {};
        };

        this.checkRule = function (element, rule) {
            if (rule) {
                return rule.test(element.value);
            }
            return true; //nothing to do

        };

        this.checkClassRule = function (element) {
            var rule = this.getClassRule(element);

            if (this.checkRule(element, rule._value)) {
                if (this.form_fail.indexOf(rule._class) >= 0) {
                    this.form_fail.splice(rule._class, 1);
                }
                return;
            }

            if (this.form_fail.indexOf(rule._class) < 0) {
                this.form_fail.push(rule._class);
            }
        };

        this.result = function () {
            return this.form_fail.length < 1;
        };
    },
    ajax: function () {
        this.config = {
            url: './',
            method: 'POST',
            headers: {
                //"Content-Type": "application/x-www-form-urlencoded"
            },
            async: true
        };

        this.react = {
            //readyState is 4
            Status: {
                200: function (data) {
                    console.log(data);
                },
                404: function (error) {
                    console.log(error);
                },
                other: function (all) {
                    console.log(all);
                }
            },

            readyState: {
                0: function (state) {
                    //0: request not initialized
                    //console.warn('伺服器未建立連線');
                },
                1: function (state) {
                    //1: server connection established
                    //console.warn('伺服器建立連線');
                },
                2: function (state) {
                    //2: request received
                    //console.warn('伺服器收到');
                },
                3: function (state) {
                    //3: processing request
                    //console.warn('伺服器處理中');
                },
                4: function (state) {
                    //4: request finished and response is ready
                    //console.warn('伺服器完成');
                }
            }
        };


        if (window.XMLHttpRequest) {
            this.connect = new XMLHttpRequest();
        } else { //for IE
            this.connect = new ActiveXObject('Microsoft.XMLHTTP');
        }


        this.act = function () {
            var react = this.react;

            this.connect.onreadystatechange = function () {
                if (typeof(react.readyState[this.readyState]) == "function") {
                    react.readyState[this.readyState](this.status);
                    if (this.readyState < 4) {
                        return;
                    }
                }

                if (typeof(react.Status[this.status]) == "function") {
                    react.Status[this.status](this.responseText);
                    return;
                }

                react.Status.other(this);
            };
        };


        this.send = function (data) {

            this.connect.open(this.config.method, this.config.url, this.config.async);

            for (var index in this.config.headers) {
                this.connect.setRequestHeader(index, this.config.headers[index]);
            }

            this.connect.send(data);
        };


        this.OBJtoURL = function (data) {
            var StrData = [];

            for (var index in data) {
                StrData.push(index + String('=') + data[index]);
            }
            return StrData.join('&');
        };
    },
    scroll: function () {
        this.lastPosition = 0;

        this.keepPosition = function (element) {
            element.scrollTop = (element.scrollHeight - element.clientHeight) - this.lastPosition;
            this.lastPosition = element.scrollHeight - element.clientHeight;
        };
    },
    share: {
        toLine: function () {
            var url = "http://line.me/R/msg/text/";
            url += this.getURL();
            url += "?u=";
            url += this.getURL();
            window.open(url);
        },
        toFB: function () {
            var url = "https://www.facebook.com/sharer/sharer.php?u=";
            url += this.getURL();
            window.open(url);
        },
        getURL: function () {
            var str = String('http://') + window.location.hostname;
            str = main.encode_utf8(str);
            return str;
        }
    },

    encode_utf8: function (s) {
        return unescape(encodeURIComponent(s));
    },
    decode_utf8: function (s) {
        return decodeURIComponent(escape(s));
    }

};