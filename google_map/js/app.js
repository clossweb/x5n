var map;
var circle = {};
var marker = {};
var _distance = 10000;
var action_type = 'circle';
var config = {
    center: {lat: 23.5948856, lng: 119.8960512},
    zoom: 9
//        mapTypeId: google.maps.MapTypeId.TERRAIN
};
var data = {};

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), config);

    // Construct the circle for each value in citymap.
    // Note: We scale the area of the circle based on the population.


    //EVENTS
    map.addListener("click", function (event) {

        select_option('circle');
        select_option('marker');

        var dataParse = {
            type: action_type
        };
        dataParse.value = ({
            circle: {
                center: {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                },
                population: _distance
            },
            marker: {
                lat: event.latLng.lat(),
                lng: event.latLng.lng()
            }
        })[action_type];

        data[event.latLng.lat() + String('_') + event.latLng.lng()] = dataParse;
        action(event.latLng.lat() + String('_') + event.latLng.lng(), dataParse);

    });

}


//FUNCTIONS
function drawCircle(key, center) {

    if (center) {
        circle[key] = new google.maps.Circle({
            strokeColor: '#555',
            strokeOpacity: 0.2,
            strokeWeight: 2,
            fillColor: '#0060FF',
            fillOpacity: 0.35,
            map: map,
            center: center.center,
            radius: (center || {radius: 0}).radius || Math.sqrt(center.population) * 100,
            editable: true,
            draggable: true
        });
        return circle[key];
    }

    if (Array.isArray(key)) {
        key.map(function (value) {
            circle[value].setMap(null);
            delete circle[value];
            delete data[value];
        });
        return key;
    }
}

function drawMarker(key, latLng) {
    if (latLng) {
        marker[key] = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: true
        });
        map.panTo(latLng);

        return marker[key];
    }

    if (Array.isArray(key)) {
        key.map(function (value) {
            marker[value].setMap(null);
            delete marker[value];
            delete data[value];
        });
        return key;
    }
}


function action(_key, _data) {

    switch (_data.type) {
        case 'circle':
            var circle_OBJ = {};

            if (circle_OBJ = drawCircle(_key, _data.value)) {

                addHandle('circle', _data.value.center.lat.toFixed(3) + String(',') + _data.value.center.lng.toFixed(3), _key);
                circle_OBJ.addListener("click", function () {
                    select_option('circle', _key);
                });
                circle_OBJ.addListener('radius_changed', function () {
                    data[_key].value.radius = circle_OBJ.getRadius();

                });
                circle_OBJ.addListener("mouseup", function (event) {
                    data[_key].value.center = {
                        lat: event.latLng.lat(),
                        lng: event.latLng.lng()
                    };
                });
            }
            break;
        case 'marker':
            var marker_OBJ = {};

            if (marker_OBJ = drawMarker(_key, _data.value)) {

                addHandle('marker', _data.value.lat.toFixed(3) + String(',') + _data.value.lng.toFixed(3), _key);
                marker_OBJ.addListener("click", function () {
                    select_option('marker', _key);
                });
                marker_OBJ.addListener("mouseup", function (event) {
                    data[_key].value = {
                        lat: event.latLng.lat(),
                        lng: event.latLng.lng()
                    };
                });
            }
            break;
    }
}

function getWhich(_circle, _marker) {
    return google.maps.geometry.spherical.computeDistanceBetween(_marker.position, _circle.center) <= _circle.getRadius();
}

function _c() {
    return circle[Object.keys(circle)[0]];
}
function _m() {
    return marker[Object.keys(marker)[0]];
}