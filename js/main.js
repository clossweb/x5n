var newElement = {list: [], page_index: []};
var cloneNode = {};
var date = new Date();

for (var l = 0; l < 10; l++) {
    newElement.list.push({
        tag: 'tr',
        content: [
            {
                tag: 'td', attr: {class: 'mdl-data-table__cell--non-numeric'},
                content: String((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear())
            },
            {tag: 'td', content: 'KFC免費炸雞'},
            {tag: 'td', content: '吃吃'},
            {tag: 'td', content: '上架'},
            {tag: 'td', content: '5'},
            {tag: 'td', content: '2'}
        ]
    });
}

for (var p = 1; p < 10; p++) {
    newElement.page_index.push({
        tag: 'li',
        content: {
            tag: 'a',
            attr: {
                href: '#' + p
            },
            content: String(p)
        }
    });
}


main.add({element: document.getElementById('EVENTS')}, newElement.list);
main.add({element: document.getElementById('pages-index').getElementsByClassName('all')[0]}, newElement.page_index);

cloneNode['pagesIndex'] = document.getElementById('pages-index').cloneNode(true);

cloneNode.pagesIndex.removeChild(cloneNode.pagesIndex.children[0]);

main.add({element: document.getElementsByClassName('page-content')[0]},
    cloneNode.pagesIndex);


function sort_list(element){
    if(main.class.has([element],'up')){
        element.innerText = '▼';
        main.class.remove([element],'up');
        return;
    }
    element.innerText = '▲';
    main.class.add([element],'up');
}